//
//  DynamicFontViewController.swift
//  DynamicFont
//
//  Created by Saravana on 09/03/21.
//

import UIKit
import Dispatch
import Zip

struct SKFontFamily: Codable {
    var fontFamily: String?
    var fonts: [SKFont]?
}

struct SKFont: Codable {
    var title:String?
    var fontName: String?
    
    var selected = false
    
    enum CodingKeys: String, CodingKey {
        case title
        case fontName
    }
}

class CustomCell: UICollectionViewCell {
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var viewBG: UIView!
    
    func setData(_ data: SKFont) {
        labelTitle.text = data.title
        
        if let font = UIFont(name: data.fontName ?? "", size: labelTitle.font.pointSize) {
            labelTitle.font = font
        }
        
        if data.selected {
            viewBG.backgroundColor = .lightGray

        } else {
            viewBG.backgroundColor = .white

        }
        
    }
    
}

class DynamicFontViewController: UIViewController {
    
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    @IBOutlet var labelS: UILabel!
    @IBOutlet var labelM: UILabel!
    @IBOutlet var labelL: UILabel!
    @IBOutlet var labelXL: UILabel!
    
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var labelProgress: UILabel!
    
    @IBOutlet var collectionViewFonts: UICollectionView!
    
    var task: URLSessionDataTask?
    var arrayFonts = [SKFontFamily]()
    
    var fileManager: FileManager!
    var documentsURL:URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//                listFontFamiles()
        fileManager = FileManager.default
        documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        getZipFont()
    }
    
    func getZipFont() {
        var request = URLRequest(url: URL(string: "https://tmpfiles.org/dl/167377/Fonts.zip")!,timeoutInterval: Double.infinity)
        request.addValue("__cfduid=d7cfcd1519085b19ee910056bba98d1181615286978", forHTTPHeaderField: "Cookie")
        request.httpMethod = "GET"
        
        task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            
            if let error = error {
                print("FAILED")
                print(String(describing: error))
                return
            }
            
            guard let data = data else {
                print("FAILED")
                print(String(describing: error))
                return
            }
            
            print("DOWNLOAD DATA")
            print(data)
            
            if let fileURL = self?.saveDateToFile(data: data) {
                print("FILE SAVED => \(fileURL.absoluteString)")
                self?.unzipFiles(fileUrl: fileURL)
            }
            
            print(String(data: data, encoding: .utf8))
        }
    }
    
    
    func saveDateToFile(data: Data) -> URL? {
        let fileName = "\(Date().timeIntervalSince1970).zip"
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let fileURL = documentDirectory.appendingPathComponent(fileName)
            try? data.write(to: fileURL)
            return fileURL
        } catch {
            print(error)
        }
        
        return nil
    }
    
    func unzipFiles(fileUrl: URL) {
        do {
            let file = try Zip.quickUnzipFile(fileUrl) { (progress) in print(progress) }
            findFonts(source: file)
        } catch  {
            print(error)
        }
        
    }
    
    func findFonts(source: URL){
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: source, includingPropertiesForKeys: nil)
            
            if let fontFolder = fileURLs.first(where: { (url) -> Bool in
                return url.absoluteString.hasSuffix("Fonts/")
            }) {
                let fontFolderContents  = try fileManager.contentsOfDirectory(at: fontFolder,
                                                                              includingPropertiesForKeys: nil,
                                                                              options: .skipsHiddenFiles)
                
                
                
                if let assetsURL = fontFolderContents.first { (url) -> Bool in return url.absoluteString.hasSuffix("assets/") } {
                    let fontAssets = try fileManager.contentsOfDirectory(at: assetsURL,
                                                                         includingPropertiesForKeys: nil,
                                                                         options: .skipsHiddenFiles)
                    for fontURL in fontAssets {
                        //                    Font registration
                        var errorRef: Unmanaged<CFError>?
                        let success = CTFontManagerRegisterFontsForURL(fontURL as CFURL,
                                                                       .process,
                                                                       &errorRef)
                        if !success && errorRef != nil{
                            print(errorRef)
                        }
                    }
                }
                
                if let mapperURL = fontFolderContents.first { (url) -> Bool in return url.absoluteString.hasSuffix("font_mapper.json") } {
                    
                    let data = try NSData(contentsOf: mapperURL, options: .mappedIfSafe)
                    let decoder = JSONDecoder()
                    let jsonData = try decoder.decode([SKFontFamily].self, from: data as Data)
                    self.arrayFonts.append(contentsOf: jsonData)
                    //                    let asd = try decoder.decode([SKFontFamily].self, from: jsonData)
                }
                
//                self.listFontFamiles()
                DispatchQueue.main.async {
                    self.indicatorView.stopAnimating()
                    self.collectionViewFonts.reloadData()
                }
            }
            
            // process files
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
    }
    
    func listFontFamiles() {
        for family in UIFont.familyNames {
            debugPrint("->> \(family)")
            for font in UIFont.fontNames(forFamilyName: family) {
                debugPrint("------>> \(font)")
            }
        }
    }
}

// MARK: - IBAction
extension DynamicFontViewController {
    
    @IBAction func startAction(_ sender: Any?) -> Void {
        indicatorView.startAnimating()
        arrayFonts.removeAll()
        collectionViewFonts.reloadData()
        task = nil
//        getZipFont()
//        task?.resume()
        
        let url = URL(string: "https://speed.hetzner.de/1GB.bin")
        
        let downloadRequest = APIDownloadRequest()
        downloadRequest.downloadDataFrom(url!) { [weak self](progress) in
            DispatchQueue.main.async {
                self?.progressView.progress = progress
                self?.labelProgress.text =  String(format: "%0.2f %%", progress * 100)
            }
        } success: { (urlFile) in
            print("")
        } failed: { (error) in
            print("")
        }

        
        
    }
    
}

// MARK: UICollectionViewDataSource
extension DynamicFontViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrayFonts.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let fonts = arrayFonts[section]
        return fonts.fonts?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let fonts = arrayFonts[indexPath.section]
        let fontData = fonts.fonts![indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell.id", for: indexPath) as! CustomCell
        cell.setData(fontData)
        
        return cell
    }
    
}

// MARK: UICollectionViewDelegate
extension DynamicFontViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var sectionData = arrayFonts[indexPath.section]
        guard let rowData = sectionData.fonts?[indexPath.item] else { return }

        
        let newRowData = sectionData.fonts?.enumerated().map({ (index, element) -> SKFont in
            if index == indexPath.item {
                return SKFont(title: element.title, fontName: element.fontName, selected: true)
            } else {
                return SKFont(title: element.title, fontName: element.fontName, selected: false)
            }
        })
        
        sectionData.fonts = newRowData
        arrayFonts[indexPath.section] = sectionData
        collectionViewFonts.reloadData()
        
        
        if let font = UIFont(name: rowData.fontName ?? "", size: labelS.font.pointSize) {
            labelS.font = font
        }
        if let font = UIFont(name: rowData.fontName ?? "", size: labelM.font.pointSize) {
            labelM.font = font
        }
        if let font = UIFont(name: rowData.fontName ?? "", size: labelL.font.pointSize) {
            labelL.font = font
        }
        if let font = UIFont(name: rowData.fontName ?? "", size: labelXL.font.pointSize) {
            labelXL.font = font
        }
        
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout
extension DynamicFontViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50)
    }
}

