//
//  NetworkManager.swift
//  DynamicFont
//
//  Created by Saravana on 12/03/21.
//

import Foundation

typealias ProgressCallback = (_ progress: Float) -> Void
typealias SuccessCallback = (_ dataURL: URL?) -> Void
typealias FailedCallback = (_ error: Error?) -> Void

class APIDownloadRequest: NSObject {
    
    var session: URLSession?
    
    private let identifier = "identifier"
    
    var progressCallback: ProgressCallback?
    var successCallback: SuccessCallback?
    var failedCallback: FailedCallback?
    
    override init() {
        super.init()
        
        let config = URLSessionConfiguration.background(withIdentifier: self.identifier)
        session = URLSession.init(configuration: config,
                                  delegate: self,
                                  delegateQueue: nil)
    }
    
    func downloadDataFrom( _ url: URL, progress: ProgressCallback?, success: SuccessCallback?, failed: FailedCallback?) {
        self.progressCallback = progress
        self.successCallback = success
        self.failedCallback = failed
        
        let downloadTask = session!.downloadTask(with: url)
        downloadTask.resume()
    }
}

// MARK: URLSessionDelegate
extension APIDownloadRequest: URLSessionDelegate {
    
}

// MARK: URLSessionTaskDelegate
extension APIDownloadRequest: URLSessionTaskDelegate {
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        print(#function)
        if let failedCallback = failedCallback {
            failedCallback(error)
        }
    }
}



// MARK: URLSessionDownloadDelegate
extension APIDownloadRequest: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if let progressCallback = progressCallback {
//            debugPrint("\( Float(totalBytesWritten) / Float(totalBytesExpectedToWrite))")
            progressCallback( Float(totalBytesWritten) / Float(totalBytesExpectedToWrite) )
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {

    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        if let successCallback = successCallback {
            successCallback(location)
        }
    }
       
}


